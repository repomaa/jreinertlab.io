require "markd"
require "yaml"
require "ecr"

SRC_DIR = "./src"
DEST_DIR = "./public"
DEFAULT_TITLE = "Jokke's Gitlab Pages"

struct Metadata
  include YAML::Serializable

  getter title : String = DEFAULT_TITLE
end

struct Page
  def initialize(@metadata : Metadata, @content : String)
  end

  ECR.def_to_s "page_template.html.ecr"
end

def render_markdown(path)
  options = Markd::Options.new(smart: true)

  File.open(path) do |file|
    metadata_yaml = file.gets("\n---\n", chomp: true) || ""
    if file.pos == file.size
      html = Markd.to_html(metadata_yaml)
      metadata_yaml = "{}"
    else
      html = Markd.to_html(file.gets_to_end, options)
    end

    metadata = Metadata.from_yaml(metadata_yaml)

    IO::Memory.new.tap do |io|
      Page.new(metadata, html).to_s(io)
      io.rewind
    end
  end
end

Dir[File.join(SRC_DIR, "**/*")].each do |path|
  if File.extname(path) == ".md"
    content = render_markdown(path)
    dest_name = "#{path[(SRC_DIR.size)..-4]}.html"
  else
    dest_name = path[(SRC_DIR.size)..-1]
    content = File.new(path)
  end

  dest_path = File.join(DEST_DIR, dest_name)
  Dir.mkdir_p(File.dirname(dest_path))

  File.open(dest_path, "w+") do |file|
    IO.copy(content, file)
  end

  content.close
end
